/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "Adherent" (
  "num" smallint(6) NOT NULL,
  "nom" varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  "prenom" varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  "adrRue" varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  "adrCP" mediumint(5) DEFAULT NULL,
  "adrVille" varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  "tel" varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  "mel" varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY ("num")
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "Animal" (
  "id" smallint(5) unsigned NOT NULL,
  "sexe" char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  "dateNaiss" datetime DEFAULT NULL,
  "nom" varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  "commentaires" text COLLATE utf8_unicode_ci,
  "espece_id" smallint(5) unsigned NOT NULL,
  "race_id" smallint(5) unsigned DEFAULT NULL,
  "mere_id" smallint(5) unsigned DEFAULT NULL,
  "pere_id" smallint(5) unsigned DEFAULT NULL,
  PRIMARY KEY ("id"),
  UNIQUE KEY "unic_nom_espece_id" ("nom","espece_id"),
  KEY "fk_espece_id" ("espece_id"),
  KEY "fk_race_id" ("race_id"),
  KEY "fk_mere_id" ("mere_id"),
  KEY "fk_pere_id" ("pere_id"),
  CONSTRAINT "fk_pere_id" FOREIGN KEY ("pere_id") REFERENCES "Animal" ("id"),
  CONSTRAINT "fk_espece_id" FOREIGN KEY ("espece_id") REFERENCES "Espece" ("id"),
  CONSTRAINT "fk_mere_id" FOREIGN KEY ("mere_id") REFERENCES "Animal" ("id"),
  CONSTRAINT "fk_race_id" FOREIGN KEY ("race_id") REFERENCES "Race" ("id")
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "Animaux" (
  "id" int(10) unsigned NOT NULL,
  "espece" varchar(20) DEFAULT NULL,
  "sexe" char(1) DEFAULT NULL,
  "dateNaiss" date DEFAULT NULL,
  "nom" varchar(20) DEFAULT NULL,
  "espece_id" smallint(5) unsigned DEFAULT NULL,
  PRIMARY KEY ("id"),
  UNIQUE KEY "UNIC_n" ("nom")
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "Auteur" (
  "num" smallint(6) NOT NULL,
  "nom" varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  "prenom" varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  "nationalite" varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY ("num")
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "Classe" (
  "code" varchar(7) NOT NULL DEFAULT '',
  "niveau" varchar(1) NOT NULL,
  "filiere" varchar(7) DEFAULT NULL,
  PRIMARY KEY ("code")
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "Eleve" (
  "num" int(10) unsigned NOT NULL,
  "nom" varchar(30) NOT NULL,
  "prenom" varchar(30) NOT NULL,
  "dateNaiss" date DEFAULT NULL,
  "sexe" varchar(1) NOT NULL,
  "codeClasse" varchar(7) NOT NULL,
  PRIMARY KEY ("num"),
  KEY "codeClasse" ("codeClasse"),
  CONSTRAINT "Eleve_ibfk_1" FOREIGN KEY ("codeClasse") REFERENCES "Classe" ("code")
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "Enseignement" (
  "numProf" int(10) unsigned NOT NULL,
  "codeClasse" varchar(7) NOT NULL DEFAULT '',
  "nbHeures" decimal(10,0) DEFAULT NULL,
  "anneeScolaire" char(9) NOT NULL DEFAULT '',
  PRIMARY KEY ("numProf","codeClasse","anneeScolaire"),
  KEY "codeClasse" ("codeClasse"),
  CONSTRAINT "Enseignement_ibfk_1" FOREIGN KEY ("numProf") REFERENCES "Professeur" ("num"),
  CONSTRAINT "Enseignement_ibfk_2" FOREIGN KEY ("codeClasse") REFERENCES "Classe" ("code")
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "Espece" (
  "id" smallint(5) unsigned NOT NULL,
  "nom_courant" varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  "nom_latin" varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  "description" text COLLATE utf8_unicode_ci,
  "prix" decimal(7,2) unsigned DEFAULT NULL,
  PRIMARY KEY ("id"),
  UNIQUE KEY "UNIC_nl" ("nom_latin")
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "Genre" (
  "num" tinyint(4) NOT NULL,
  "libelle" varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY ("num")
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "Livre" (
  "num" smallint(6) NOT NULL,
  "ISBN" varchar(13) COLLATE utf8_unicode_ci NOT NULL,
  "titre" varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  "prix" float DEFAULT NULL,
  "editeur" varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  "annee" smallint(4) DEFAULT NULL,
  "langue" varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  "numAuteur" smallint(6) DEFAULT NULL,
  "numGenre" tinyint(4) DEFAULT NULL,
  PRIMARY KEY ("num"),
  KEY "livre_fk_1" ("numAuteur"),
  KEY "livre_fk_2" ("numGenre"),
  CONSTRAINT "livre_fk_1" FOREIGN KEY ("numAuteur") REFERENCES "Auteur" ("num"),
  CONSTRAINT "livre_fk_2" FOREIGN KEY ("numGenre") REFERENCES "Genre" ("num")
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "Pret" (
  "num" smallint(6) NOT NULL,
  "numLivre" smallint(6) NOT NULL DEFAULT '0',
  "numAdherent" smallint(6) NOT NULL DEFAULT '0',
  "datePret" date NOT NULL DEFAULT '0000-00-00',
  "dateRetourPrevue" date NOT NULL DEFAULT '0000-00-00',
  "dateRetourReelle" date DEFAULT NULL,
  PRIMARY KEY ("num"),
  KEY "pret_fk_1" ("numLivre"),
  KEY "pret_fk_2" ("numAdherent"),
  CONSTRAINT "pret_fk_1" FOREIGN KEY ("numLivre") REFERENCES "Livre" ("num"),
  CONSTRAINT "pret_fk_2" FOREIGN KEY ("numAdherent") REFERENCES "Adherent" ("num")
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "Professeur" (
  "num" int(10) unsigned NOT NULL,
  "civ" varchar(3) DEFAULT NULL,
  "nom" varchar(20) NOT NULL,
  "discipline" varchar(20) NOT NULL,
  "prenom" varchar(20) DEFAULT NULL,
  PRIMARY KEY ("num")
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "Race" (
  "id" smallint(5) unsigned NOT NULL,
  "nom" varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  "espece_id" smallint(5) unsigned NOT NULL,
  "description" text COLLATE utf8_unicode_ci,
  "prix" decimal(7,2) unsigned DEFAULT NULL,
  PRIMARY KEY ("id"),
  KEY "fk_race_espece_id" ("espece_id"),
  CONSTRAINT "fk_race_espece_id" FOREIGN KEY ("espece_id") REFERENCES "Espece" ("id")
);
/*!40101 SET character_set_client = @saved_cs_client */;
