% Transfert de données MySQL -> PostgreSQL
%
% Auteur  : Grégory DAVID
%%

\documentclass[12pt,a4paper,oneside,titlepage,final]{article}

\usepackage{style/layout}
\usepackage{style/glossaire}

\newcommand{\MONTITRE}{Transfert de données et structures}
\newcommand{\MONSOUSTITRE}{depuis MySQL vers PostgreSQL}
\newcommand{\DISCIPLINE}{\glsentrytext{SiSIX} -- \glsentrydesc{SiSIX}}

\usepackage[%
	pdftex,%
	pdfpagelabels=true,%
	pdftitle={\MONTITRE},%
	pdfauthor={Grégory DAVID},%
	pdfsubject={\MONSOUSTITRE},%
        colorlinks,%
]{hyperref}
\usepackage{style/commands}
\addbibresource{\jobname.bib}

\usepackage{booktabs}
\usepackage{amsfonts}

\title{%
  \begin{flushright}
      \noindent{\Huge {\bf \MONTITRE}} \\
      \noindent{\huge \MONSOUSTITRE} \\
      \noindent{\large \DISCIPLINE} \\
  \end{flushright}
}

%\printanswers\groolotPhiligranne{CORRECTION}

\begin{document}
% Page de titre
\maketitle

% Copyright
\include{copyright}

% Contenu
\HEADER{\MONTITRE}{\DISCIPLINE}

\section*{Pourquoi convertir ?}
Les implémentations du langage SQL son incompatibles entre les
différents distributeurs et n'ont aucune obligation à respecter le
standard\footnote{\url{https://en.wikipedia.org/wiki/SQL}}. Cependant,
PostgreSQL et Mimer SQL ont pour objectif d'être pleinement
compatibles avec la norme, ce qui fait d'eux les SGBDR les plus
interopérables des solutions.

Cette compatibilité et ce respect du standard fait de PostgreSQL le
choix privilégié pour notre apprentissage.

\section*{Éléments de conversion}
Cette documentation s'inspire en partie de la lecture des
documentations respectives de MySQL et PostgreSQL, ainsi que du livre
numérique \textcite{convert_mysql_to_postgresql}.

Un exemple de remplacement\footnote{dans le monde des Expressions
  Rationnelles, on parle de substitution} global\footnote{d'où le
  \texttt{g} en fin d'expression} par expression
rationnelle\footnote{souvent aussi appelée \emph{Regular Expression}
  ou \emph{RegExp}} de la chaîne de caractères \texttt{smallint(n)}
par \texttt{smallint}, où \texttt{n} est un nombre positif sur un ou
plusieurs chiffres, pourrait être réalisé par la commande :

\texttt{sed -E -e 's/smallint$\backslash$(.+$\backslash$)/smallint/g'}
\rowcolors{2}{white}{black!15}
\begin{table}[h]
  \begin{minipage}{1.0\linewidth}
    \centering \resizebox{\textwidth}{!}{
      \begin{tabular}{llp{7cm}p{7cm}}
        \toprule
        MySQL & PostgreSQL & Description & RegExp de recherche (et remplacement si $\Rightarrow$) \\
        \midrule
        \texttt{"} & \texttt{'} & MySQL utilise indistinctement \texttt{'} ou  \texttt{"} pour entourer les valeurs, tandis que PostgreSQL utilise ce que la norme spécifie : \texttt{'} & \texttt{"} \\
        \texttt{`} & \texttt{"} & Encadrement des noms de table et de champs avec des \emph{back quotes} \texttt{`} dans MySQL, ce qui n'est pas standard. PostgreSQL attend les \emph{double quotes} \texttt{"} & \texttt{`} \\
        \texttt{smallint(n)} & \texttt{smallint} & Le standard SQL défini le type \texttt{smallint} comme ne prenant aucun paramètre de longueur\footnote{voir \url{https://www.postgresql.org/docs/9.4/static/datatype-numeric.html}} & \texttt{smallint$\backslash$([0-9]+$\backslash$)} \\
        \texttt{mediumint(n)} & \texttt{integer} & Le type \texttt{mediumint} n'existe pas & \texttt{mediumint$\backslash$([0-9]+$\backslash$)} \\
        \texttt{float(n)} & \texttt{real} & SQL définit le type \texttt{float4}, mais PostgreSQL implémente le type \texttt{real} & \texttt{float($\backslash$([0-9]+$\backslash$))?} \\
        \texttt{decimal(n, m)} & \texttt{numeric(n, m)} & \texttt{decimal} n'est pas SQL standard & \texttt{decimal} \\
        \texttt{datetime} & \texttt{timestamp} & \texttt{datetime} n'est pas SQL standard & \texttt{datetime} \\
        \texttt{unsigned} & N/A\footnote{La valeur \emph{N/A} correspond à \emph{Not Applicable} ou bien \emph{Not Available}} & SQL n'accepte aucune spécialisation du type comme n'étant pas signé. PostgreSQL respecte cela & \texttt{unsigned} \\
        \texttt{KEY \dots} & N/A & \texttt{KEY} est une contrainte de table de création d'index. Par conséquent il faut se référer à la notation dans PostgreSQL\footnote{voir \url{https://www.postgresql.org/docs/9.4/static/sql-createtable.html}} et donc les supprimer des créations de table pour en faire des requêtes à part entière & \texttt{KEY "index" ("index\_colonne")} $\Rightarrow$ \newline \texttt{CREATE INDEX index ON nom\_de\_table (index\_colonne)} \\
        \texttt{/* \dots{} */;} & N/A & Les commentaires ne sont pas utiles pour le transfert, d'autant qu'ils sont spécifiques à MySQL & \texttt{s|/$\backslash$*.+$\backslash$*/||g} \\
        \texttt{utf8\_unicode\_ci} & \texttt{"fr\_FR"} & L'ensemble du serveur PostgreSQL est configuré pour faire de l'\texttt{UTF8}, il suffit donc de spécifier la collation acceptable & \texttt{utf8\_unicode\_ci} \\
        \texttt{||} et \texttt{\&\&} & \texttt{OR} et \texttt{AND} & Les opérateurs standards de comparaison logique sont \texttt{AND} et \texttt{OR}. Les deux SGBDR les reconnaissent, MySQL est plus permissif en acceptant aussi les opératreurs logiques du labgage C. Attention, dans PostgreSQL \texttt{||} est l'opérateur de concaténation de chaînes & \texttt{||} $\Rightarrow$ \texttt{OR} \newline et\newline \texttt{\&\&} $\Rightarrow$ \texttt{AND} \\
        \bottomrule
      \end{tabular}
    }
  \end{minipage}
  \caption{Liste des éléments à convertir depuis MySQL vers
    PostgreSQL}
  \label{tab:mysql.to.postgresql}
\end{table}

% References bibliographiques
\clearpage %
\printbibheading %
\printbibliography[nottype=online,check=notonline,heading=subbibliography,title={Bibliographiques}] %
\printbibliography[check=online,heading=subbibliography,title={Webographiques}] %

\printglossaries %

\end{document}

